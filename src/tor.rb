# Copyright (c) 2021 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

module TorPlugin
  # Returns a single random sample from the values found in array, but exclude
  # any values found in exclude_array.
  def random_sample(array, exclude_array=[])
    value = array - exclude_array
    value.sample
  end

  # Returns a random reviewer found in the reviewers argument, but exclude the
  # author of the MR.
  def random_reviewer(reviewers)
    random_sample(reviewers.select {|reviewer| !busy(reviewer) }, [author[:username]])
  end

  # Returns whether user is currently busy
  def busy(user)
    url = build_url(options: { source: 'users',
                               source_id: user,
                               resource_type: 'status'})
    network.query_api_cached(url)[0][:availability] == "busy"
  end

  # Get some additional informations about a MR
  def mr_info
    url = build_url(options: { source: 'projects',
                               source_id: resource[:project_id],
                               resource_type: 'merge_requests',
                               resource_id: resource[:iid]})
    network.query_api_cached(url)[0]
  end

  # returns the pipeline status, mapping some status to their closest relative
  def pipeline_status
    case mr_info[:head_pipeline][:status]
    when "created", "waiting_for_resource", "preparing", "pending", "running", "manual", "scheduled"
      "running"
    when "success", "skipped"
      "success"
    when "failed", "canceled"
      "failed"
    end
  end

  # returns the timestamp at which the pipeline ended, or nil if it's still
  # ongoing
  def pipeline_end_time
    str_time = mr_info[:head_pipeline][:finished_at]
    DateTime.iso8601(str_time) unless str_time.nil?
  end

  # returns the timestamp at which the bot last interacted with the MR.
  # If the bot never interacted, returns UNIX epoch.
  def bot_last_interacted
    last_interacted(false) || DateTime.new(1970)
  end

  # returns whether the bot interacted at least once with the MR.
  def bot_interacted?
    !last_interacted(false).nil?
  end

  # returns the timestamp at which someone who is not the bot last interacted
  # with the mr. If no-one interacted yet, returns the MR creation time.
  def person_last_interacted
    last_interacted(true) || DateTime.iso8601(resource[:created_at])
  end

  # if invert is false, returns the last time someone in $users interacted with
  # the MR. If inver is true, returns the last time someone not in $users
  # interatect instead. If no interaction matches, returns nil.
  def last_interacted(invert, users = ['triage-bot'])
    url = build_url(options: { source: 'projects',
                               source_id: resource[:project_id],
                               resource_type: 'merge_requests',
                               resource_id: resource[:iid],
                               sub_resource_type: 'discussions'})
    discussions = network.query_api_cached(url)

    discussions.flat_map { |e| e[:notes] }
      .select {|msg| users.include?(msg[:author][:username]) != invert}
      .map {|msg| DateTime.iso8601(msg[:updated_at])}
      .max
  end

  # Returns the author of a given issue or MR.
  def author
    resource[:author]
  end

  # Returns the assignee of a given issue or MR.
  def assignee
    resource[:assignee]
  end

  # Returns true if the given issue or MR has an assignee.
  def has_assignee?
    !assignee.nil?
  end

  # Returns the list of reviewers.
  def reviewers
    resource[:reviewers]
  end

  # Returns true iff the given MR have a reviewer.
  def has_reviewer?
    !reviewers.empty?
  end

  # Returns true iff the given MR is Work-in-progress/Draft
  def draft?
    resource[:draft]
  end

  # Show the resource data and return value as success indicator.
  def debug(value)
    puts "=== DEBUG ==="
    puts "#{resource}"
    puts "============="

    value
  end
end

Gitlab::Triage::Resource::Context.include TorPlugin
