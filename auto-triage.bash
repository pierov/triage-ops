#!/usr/bin/env bash

set -e

# Path to where our policies can be found.
POLICY_DIRECTORY=policies

# Dry-run?
DRY_RUN=false

# Debug?
DEBUG=false

# Gitlab Hostname
GITLAB_HOST_URL="https://gitlab.torproject.org/"

# Gitlab Token.
GITLAB_TOKEN=""

# Parse arguments.
for arg in "${@}" ; do
    case "${arg}" in
        --dry-run)
            DRY_RUN=true
            shift
            ;;

        --debug)
            DEBUG=true
            shift
            ;;

        --token=*)
            GITLAB_TOKEN=${arg#*=}
            shift
            ;;
    esac
done

# Handle options that we forward to gitlab-triage.
gitlab_triage_options=""

if ${DEBUG} ; then
    gitlab_triage_options="${gitlab_triage_options} --debug"
fi

if ${DRY_RUN} ; then
    gitlab_triage_options="${gitlab_triage_options} --dry-run"
fi

ERROR=false

for policy in $(find ${POLICY_DIRECTORY} -follow -type f -name '*.yaml' -not -path '*/\.*' | sort -f) ; do
    gitlab_project_name=$(dirname "${policy#${POLICY_DIRECTORY}/}")

    echo "Triaging '${gitlab_project_name}' using the policy found in '${policy}' ..."

    if ! gitlab-triage                     \
        --require ./src/tor.rb             \
        --source-id ${gitlab_project_name} \
        --host-url ${GITLAB_HOST_URL}      \
        --policies-file ${policy}          \
        --token ${GITLAB_TOKEN}            \
        ${gitlab_triage_options} ; then
        echo "Error while running gitlab-triage. Giving up :-("
        ERROR=true
    fi
done

if $ERROR ; then
    echo "An error happened during this run. Check logs."
    exit 1
fi
